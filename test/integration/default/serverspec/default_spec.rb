require 'spec_helper'

describe 'apache::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html
  describe package('httpd') do
    it { should be_installed }
  end
  describe service('httpd') do
    it { should be_enabled }
  end
  describe service('httpd') do
    it { should be_running }
  end
  describe port(80) do
    it { should be_listening.with('tcp') }
  end
  describe file('/var/www/html/index.html') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 644 }
    its (:content) {should match /Hello, World!/}
  end
end
