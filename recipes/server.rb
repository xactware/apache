package 'Install Apache' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name  'httpd'
  when 'ubuntu', 'debian'
    package_name  'apache2'
  action :install
  end
end

file '/var/www/html/index.html' do
  content '<h1>Hello, World!</h1>'
  mode '0644'
  owner 'root'
  group 'root'
  action :create
end

service 'httpd' do
  action [ :enable, :start ]
end
